import os

from csv_loader import CSVLoader
from database import Database
from query_builder import QueryBuilder
from watcher import FileWatcher


class Importer:

    def __init__(self, args, conf) -> None:
        super().__init__()
        self.args = args
        self.conf = conf
        self.file_watcher = FileWatcher()
        self.database = Database(self.args.ip, self.args.user, self.args.password, self.args.database)
        self.query_builder = QueryBuilder(self.conf.get('MAPPING', None), self.conf.get('CONST', None))

    def process_file(self, filename):
        try:
            result = self.database.execute(self.query_builder.generate_sql(CSVLoader.load_csv_file(filename)))
            if result:
                os.remove(filename)
                print(filename + ' has been processed')
        except Exception as e:
            print(filename + ' ' + str(e))
            # TODO: send email via SMTP

    def start(self, folder, timeout):
        self.file_watcher.start(folder, self.process_file, timeout)
