from Importer import Importer
from configuration import Configuration

if __name__ == '__main__':
    args = Configuration.get_parsed_args()
    conf = Configuration.load_configuration(args.cfg)
    importer = Importer(args, Configuration.load_configuration(args.cfg))
    importer.start(args.folder, args.timeout)
