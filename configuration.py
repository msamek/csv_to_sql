import argparse
import json


class Configuration:
    @staticmethod
    def get_parsed_args():
        parser = argparse.ArgumentParser(description='Process some integers.')
        parser.add_argument('-i', '--ip', type=str, default='localhost')
        parser.add_argument('-u', '--user', type=str, default='root')
        parser.add_argument('-p', '--password', type=str, default='')
        parser.add_argument('-d', '--database', type=str, default='vlada')
        parser.add_argument('-f', '--folder', type=str, default='D:\\PersonalDevel\\CSVtoSQL')
        parser.add_argument('-c', '--cfg', type=str, default='configuration.json')
        parser.add_argument('-t', '--timeout', type=int, default=10)

        return parser.parse_args()

    @staticmethod
    def load_configuration(conf):
        with open(conf) as json_file:
            return json.load(json_file)
