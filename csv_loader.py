import csv


class CSVLoader:
    @staticmethod
    def load_csv_file(filename, from_row=1):
        with open(filename, mode='r') as infile:
            for _ in range(from_row):
                next(infile, None)
            return dict(enumerate(csv.DictReader(infile)))
