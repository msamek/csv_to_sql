import pymysql


class Database:

    def __init__(self, ip, user, password, db) -> None:
        super().__init__()
        self.ip = ip
        self.user = user
        self.password = password
        self.db = db

    def execute(self, sql):
        if not sql:
            raise Exception('SQL list is empty')

        db = pymysql.connect(self.ip, self.user, self.password, self.db)
        try:
            cursor = db.cursor()
            for query in sql:
                cursor.execute(query)
            db.commit()
            return True
        finally:
            db.close()
