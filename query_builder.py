import datetime
import time
from typing import OrderedDict


def datetotimestamp(data):
    return int(time.mktime(datetime.datetime.strptime(data, "%d.%m.%Y").timetuple()))


def commatodot(data):
    return data.replace(',', '.')


class QueryBuilder:
    def __init__(self, mapping, const) -> None:
        super().__init__()
        self.mapping = mapping
        self.const = const

    def generate_sql(self, csv_data):
        sql = []
        if not self.mapping:
            return sql

        for csv_row in csv_data.values():
            for table_name, mapping in self.mapping.items():
                query = self._match_columns(csv_row, mapping)
                if query:
                    self._add_const_values(query, table_name)
                    sql.append(self.__build_insert(table_name, query))
        return sql

    def _add_const_values(self, query, table_name):
        if self.const and table_name in self.const:
            const = self.const[table_name]
            for key, value in const.items():
                query[key] = str(value)

    @staticmethod
    def _match_columns(csv_row, mapping):
        query = OrderedDict()
        for column_name, column_prop in mapping.items():
            if column_name in csv_row and csv_row[column_name]:
                item = csv_row[column_name]
                if 'convert' in column_prop and column_prop['convert'] and column_prop['convert'] in globals():
                    item = globals()[column_prop['convert']](item)
                query[column_prop['column']] = str(item)
        return query

    @staticmethod
    def __build_insert(table_name, query):
        return 'INSERT INTO ' + table_name + ' (' + ", ".join(query.keys()) + ") VALUES " + "(" + ", ".join(
            "'{0}'".format(q) for q in query.values()) + ");"
