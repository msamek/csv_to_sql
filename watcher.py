import time
from watchdog.observers import Observer

from watchdog.events import LoggingEventHandler


class FileTypeEventHandler(LoggingEventHandler):

    def __init__(self, callback, file_type='.csv') -> None:
        super().__init__()
        self.callback = callback
        self.file_type = file_type

    def on_created(self, event):
        super().on_created(event)
        if event.src_path.endswith(self.file_type):
            self.callback(event.src_path)


class FileWatcher:
    def __init__(self) -> None:
        super().__init__()
        self.observer = Observer()

    def start(self, folder, callback, timeout):
        self.observer.schedule(FileTypeEventHandler(callback), folder, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(timeout)
        except KeyboardInterrupt:
            self.observer.stop()
        self.observer.join()
